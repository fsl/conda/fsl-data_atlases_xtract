#!/usr/bin/env bash

set -e

repository=https://git.fmrib.ox.ac.uk/fsl/data_atlases_xtract.git
version=${PKG_VERSION}

# Clone the repository, but don't
# download lfs files yet
GIT_LFS_SKIP_SMUDGE=1 git clone ${repository}

cd data_atlases_xtract
git checkout ${version}

# Download lfs files for ${version}
git lfs install --force --local
git lfs fetch
git lfs checkout

mkdir -p      $PREFIX/data/atlases
cp -r *       $PREFIX/data/atlases/
chmod -R 0755 $PREFIX/data/atlases/*
